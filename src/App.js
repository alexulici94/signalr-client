import React, { Component } from 'react';
import * as signalR from '@aspnet/signalr';
import './App.css';

const args_storage = 'args';

const getFromLocal = (key) =>{
  var value = localStorage.getItem(key)
  if (value) {
    return JSON.parse(value);
  }
  else {
    return [];
  }
}

class App extends Component {
  state = {
    args: getFromLocal(args_storage),
    newArg: "",
    hubUrl: localStorage.getItem('huburl'),
    methodName: localStorage.getItem('methodname'),
    listening: false
  }

  _handleKeyPress = (e) => {
    if (e.key === 'Enter') {
      var newArray = this.state.args.slice();    
      newArray.push(this.state.newArg);
      this.setState({args:newArray})
      localStorage.setItem(args_storage, JSON.stringify(newArray));
    }
  }

  updateArgsInputValue(evt) {
    const value = evt.target.value

    this.setState({
      newArg: value
    });
  }

  updateMethodNameInputValue(evt) {
    const value = evt.target.value;

    localStorage.setItem('methodname', value);
    this.setState({
      methodName: value
    });
  }

  updateHubUrlInputValue(evt) {
    const value = evt.target.value

    localStorage.setItem('huburl', value);
    this.setState({
      listening: false,
      hubUrl: value
    });
  }

  ConnectionButtonPressed = () => {
    this.setState(prevState => ({
      listening: !prevState.listening
    }));
  }

  render() {
    if(this.state.listening===true)
    {
      const connection = new signalR.HubConnectionBuilder()
        .withUrl(this.state.hubUrl)
        .configureLogging(signalR.LogLevel.Information)
        .build();
      connection.start().catch(err => console.error(err.toString()));

      connection.on(this.state.methodName, item => {
        console.log(item);
      });
    }

    return (
      <div className="App">
        <div>Hub Url</div>
        <input type="text" name="hub" value={this.state.hubUrl} onChange={evt => this.updateHubUrlInputValue(evt)} />
        <div>Method Name</div>
        <input type="text" name="methodname" value={this.state.methodName} onChange={evt => this.updateMethodNameInputValue(evt)} />
        <div>New Argument</div>
        <input type="text" name="args" onChange={evt => this.updateArgsInputValue(evt)} onKeyPress = {this._handleKeyPress} />
        {this.state.args.map((item, index) => (
          <div key={index}>{item}</div>
          )
        )}
        <input type="button" name="connection" value="Connect" onClick={this.ConnectionButtonPressed.bind(this)}/>
      </div>
    );
  }
}

export default App;
